<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="header.css">
    <link rel="shortcut icon" href="./Assets/img/Logo.png" type="image/x-icon">
</head>

<body>
    <div class="container-navbar">
        <div class="navbar">
            <a id="logo" href="index.php"><img src="./Assets/img/Logo.png" alt="logo"></a>
            <a href="index.php"><img src="./Assets/img/home.png" alt="home"><p>HOME</p></a>
            <a href="concept.php"><img src="./Assets/img/concept.png" alt="concept"><p>NOS CONCEPT</p></a>
            <a href="reservation-cinema.php"><img src="./Assets/img/réserver.png" alt="reserver"><p>RÉSERVATION</p></a>
            <a href="geo.php"><img src="./Assets/img/cinéma.png" alt="cinema"><P>NOS CINÉMAS</P></a>
            <a href="contact.php"><img src="./Assets/img/contact.png" alt="contact"><p>NOUS CONTACTER</p></a>
        </div>
    </div>

    <div class="container-navbar1">
        <div class="navbar1">
            <a href="#" class="menuBurger"><img src="./Assets/img/menu-burger.png" alt="burger" width="70%" height="auto"></a>
            <a href="index.php"><img src="./Assets/img/Logo.png" alt="logo" width="70%" height="auto"></a>
        </div>
    </div>
</body>
    <script>
        const menuHamburger = document.querySelector(".menuBurger")
        const navLinks = document.querySelector(".container-navbar")

        menuHamburger.addEventListener('click',()=>{
            navLinks.classList.toggle('mobile-menu')
        })
    </script>
</html>