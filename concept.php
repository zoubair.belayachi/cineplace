<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="concept.css">
    <title>Concept</title>
</head>

<body>
    <div class="container-page">
        <?php include "header.php" ?>
        <div class="column">
            <div class="hero">
                <div class="bande">
                    <div class="rouge">
                        <h2>CINÉPLACE</h2>
                    </div>
                    <div class="bleu">
                        <h2>CONCEPT</h2>
                    </div>
                </div>
            </div>
            <div class="section un">
                <img class="spider" src="./Assets/img/spiderman.png" alt="spiderman" width="40%">
                <div class="rectangle rouge r1 dec1"></div>
                <div class="rectangle bleu r1"></div>
                <div class="case1">
                    <p class="case-txt">Cinéplaces est un site permettant de découvrir facilement les films projetés dans les salles de cinéma près de chez vous.</p>
                </div>
            </div>

            <div class="section deux">
                <img class="superman" src="./Assets/img/superman.png" alt="superman" width="40%">
                <div class="rectangle rouge r2 "></div>
                <div class="rectangle bleu r2 dec2"></div>
                <div class="case2">
                    <p class="case-txt">Le site offre également la possibilité de calculer le prix global pour une famille pour partager un film ensemble. </p>
                </div>
            </div>

            <div class="section trois">
                <img class="captain" src="./Assets/img/captain-america.png" alt="captain" width="40%">
                <div class="rectangle rouge r1 dec1"></div>
                <div class="rectangle bleu r1"></div>
                <div class="case1">
                    <p class="case-txt">Enfin vous et c’est bien là toute l’originalité de Cinéplace, vous pouvez commander votre PopCorn depuis le site est-il vous attendra à votre place.</p>
                </div>
            </div>

            <div class="section">
                <img class="captain-marvel" src="./Assets/img/captain-marvel.png" alt="captain marvel" width="40%">
                <div class="rectangle rouge r2 dec2"></div>
                <div class="rectangle bleu r2"></div>
                <div class="case2">
                    <p class="case-txt">Tout est plus simple avec Cinéplace.</p>
                </div>
            </div>
            <div class="contain-page">
                <h1>NOTRE CONCEPT</h1>
                <div class="ligne"></div>
                <p>Cinéplaces est un site permettant de découvrir facilement les films projetés dans les salles de cinéma près de chez vous.
                Le site offre également la possibilité de calculer le prix global pour une famille pour partager un film ensemble.
                Enfin vous et c’est bien là toute l’originalité de Cinéplace, vous pouvez commander votre PopCorn depuis le site est-il vous attendra à votre place.
                Tout est plus simple avec Cinéplace.
                </p>
            </div>


        </div>
    </div>
 

</body>
<?php include "footer.php" ?>

</html>