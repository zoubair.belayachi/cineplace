<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="footer.css">
    <title>Document</title>
</head>
<body>
<body>
    <div class="container-footer">

        <div class="navbar-foot">
            <div class="ligne"></div>
            <div class="navbar-page">
                <a href="index.php">HOME</a>
                <a href="concept.php">CONCEPT</a>
                <a href="reservation-cinema.php">RÉSERVATION</a>
                <a href="geo.php">CINÉMAS</a>
                <a href="contact.php">CONTACT</a>
            </div>
            <div class="ligne"></div>
            <div class="mention">
                <a href="#">CGU</a>
                <a href="#">CGV</a>
                <a href="#">MENTIONS</a>
            </div>
            <div class="ligne"></div>
        </div>

        <div class="logo">
            <img src="./Assets/img/Logo.png" alt="Logo">
        </div>

        <div class="reseaux">
            <div class="reseaux-txt">
                <H2>RÉSEAUX</H2>
            </div>
            <div class="ligne"></div>
            <div class="icon-res">
                <div><img src="./Assets/img/twitter.png" alt="twitter"></div>
                <div><img src="./Assets/img/insta.png" alt="insta"></div>
                <div><img src="./Assets/img/facebook.png" alt="facebook"></div>
            </div>
        </div>
    </div>
    <div class="copy">
        <h2>COPYRIGHT</h2>
    </div>

</body>
</body>
</html>