<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="footer.css">
    <title>Accueil</title>
</head>

<body>
    <div class="container-page">
        <?php include "header.php" ?>
        <div class="column">
            <div class="hero">
                <div class="bande">
                    <div class="rouge">
                        <h2>CINÉPLACE</h2>
                    </div>
                    <div class="bleu">
                        <h2>MOVIE</h2>
                    </div>
                </div>
                <div class="button">
                    <a href="reservation-cinema.php"><button>RÉSERVEZ</button></a>
                </div>
            </div>

            <div class="contain-page">
                <h2>A L'AFFICHE</h2>
                <div class="ligne">

                </div>

                <div class="contain-cards">
                    <div class="cards">
                        <img src="./Assets/img/chamboultout.png" alt="chamboultou" width="100%">
                        <p>Chamboultout</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/mon-bébé.png" alt="mon bebe" width="100%">
                        <p>Mon bébé</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/le-mystère.png" alt="le mystère de henri pick" width="100%">
                        <p>Le Mystère Henri Pick</p>
                    </div>
                </div>
                <h2>FILMS EN 3D</h2>
                <div class="ligne">

                </div>

                <div class="contain-cards">
                    <div class="cards">
                        <img src="./Assets/img/marvel.png" alt="marvel" width="100%">
                        <p>Captain Marvel</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/shazam.png" alt="shazam" width="100%">
                        <p>Shazam!</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/dragon.png" alt="dragons 3" width="100%">
                        <p>Dragons 3 : Le monde caché</p>
                    </div>
                </div>
                <h2>FILMS FRANCAIS</h2>
                <div class="ligne">

                </div>

                <div class="contain-cards">
                    <div class="cards">
                        <img src="./Assets/img/walter.png" alt="walter" width="100%">
                        <p>Walter</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/le-chant-loup.png" alt="le chant loup" width="100%">
                        <p>Le chant du loup</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/questce.png" alt="fait au bon dieu" width="100%">
                        <p>Qu'est ce qu'on a fait au bon dieu ?</p>
                    </div>
                                    </div>
                <h2>FILMS</h2>
                <div class="ligne">

                </div>

                <div class="contain-cards margin-b">
                    <div class="cards">
                        <img src="./Assets/img/us.png" alt="us" width="100%">
                        <p>US</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/dbz.png" alt="dragon ball super" width="100%">
                        <p>Dragon ball Super Broly</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/captive.png" alt="captive" width="100%">
                        <p>Captive State</p>
                    </div>

                    <div class="cards">
                        <img src="./Assets/img/green.png" alt="green book" width="100%">
                        <p>Green Book</p>
                    </div>
                </div>
                </div>
                
            </div>

        </div>


    </div>


</body>
<?php include "footer.php" ?>

</html>

<?php
$user = 'root';
$pass = 'root';

try {
  $db = new PDO('mysql:host=localhost;dbname=formulaire',$user,$pass);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo "Erreur : la connexion a echoue :" . $e->getMessage() . "</br>";
  die;
}

if(isset($_POST['envoyer']))
{
  $nom = $_POST['name'];
  $prenom = $_POST['firstname'];
  $email = $_POST['email'];
  $sujet = $_POST['subject'];
  $messages = $_POST['message'];

  $sql = ("INSERT INTO Contact (Nom,Prenom,Email, Motif,Messages) VALUES (:nom, :prenom, :email, :sujet, :messages)");
  $stmt = $db->prepare($sql);
  $stmt->bindParam(':nom', $nom);
  $stmt->bindParam(':prenom', $prenom);
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':sujet', $sujet);
  $stmt->bindParam(':messages', $messages);

  if($stmt->execute()){
    ?><script> 
    alert("Votre message à bien été envoyé.")</script> <?php
  }else{
    echo "Votre message n'a pas été envoyé réessayer";
    ?>
    <p>Cliquez <a href="formulaire.php">ici</a> pour réessayer  </p> 
    <?php
  }


}?>
