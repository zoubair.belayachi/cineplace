<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="contact.css">
    <link rel="stylesheet" href="footer.css">
    <title>Contact</title>
</head>

<body>
    <div class="container-page">
        <?php include "header.php" ?>
        <div class="column">
            <div class="hero">
                <div class="bande">
                    <div class="rouge">
                        <h2>CINÉPLACE</h2>
                    </div>
                    <div class="bleu">
                        <h2>MOVIE</h2>
                    </div>
                </div>
            </div>

            <div class="contain-page">
                <h2>CONTACT</h2>
                <div class="ligne">
                </div>

                <div class="contain-form">
                <form action="index.php" method="POST">
    <!-- NOM/PRENOM -->
    <div class="formulaire">
        <div class="cont">
        <!-- NOM -->
        <div class="part">
            <div>
            <label for="inputname"></label>

            </div>

            <input placeholder="Nom" type="text" name="name" class="form-control" id="inputname" required>

 
        </div>

        <!-- PRENOM -->
        <div class="part">
            <div>
            <label for="inputfirstname"></label>
            </div>

            <input placeholder="Prénom" type="text" name="firstname" class="form-control" id="inputfirstname" required>

        </div>

        </div>

        <div class="cont">
        <div>
            <!-- EMAIL -->
            <div class="part2">
                <div>            
                <label for="inputemail"></label>
            </div>
      
                <input placeholder="Email" type="text" name="email" class="form-control" id="inputemail" required>


            </div>
            <!-- SUJET -->
            <div class="part2">
                <div>
                <label for="inputemail"></label>

                </div>
                <input placeholder="Motif du message" type="text" name="subject" class="form-control" id="inputsubject" required>
            </div>

        </div>



        </div>


        </div>
        <div class="part-end">
            <div>
            <label for="inputemail"></label>

            </div>
            <textarea placeholder="Votre message..." type="text" name="message" class="form-control" id="inputmessage" required></textarea>

        
            <!-- MESSAGE -->

            <!-- button -->
        <div class="submit">
            <button id="btn" type="submit"  name="envoyer">Envoyer</button>
        </div>
        </div>
    </form>
                </div>

            </div>

        </div>


    </div>


</body>
<?php include "footer.php" ?>

</html>