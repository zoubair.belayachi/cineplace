<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Choisis ton cinéma</title>

    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="tarif.css">
    <link rel="stylesheet" href="header.css">


</head>

<body>
<div class="container-page">
        <?php include "header.php" ?>
        <div class="column">
            <div class="hero">
                <div class="bande">
                    <div class="rouge">
                        <h2>CINÉPLACE</h2>
                    </div>
                    <div class="bleu">
                        <h2>RÉSERVER</h2>
                    </div>
                </div>
                <div class="bande1">
                    <div class="noir">
                        <h2> CINÉMA </h2>
                    </div>
                    <div class="blanc">
                        <h2>FILM</h2>
                    </div>
                    <div class="noir">
                        <h2>TARIF</h2>
                    </div>
                </div>
                
            </div>
            <div class="retour">
                <a href="reservation-film.php"><img src="./Assets/img/icon-retour.png" alt="icon retour" width="5%"></a>
            </div>
            <div class="contain-page">
                <h1>EN MAINTENANCE</h1>
                <div class="ligne">
                </div>
            </div>

        </div>
    </div>

</body>
<?php require "footer.php"?>
</html>